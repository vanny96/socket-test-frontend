import io = require('socket.io-client');

export class SocketClient{
    playerNumber: number;
    requestRate: number;

    constructor(playerNumber: number, requestsPerSecond: number){
        this.playerNumber = playerNumber;
        this.requestRate = 1000 / requestsPerSecond;
        this.setMainSocket();
    }

    setMainSocket = () => {
        const socket = io.connect('http://localhost:3000/main-room');

        socket.on('change-namespace', (values: any) => {
            this.setRoomSocket(values.namespace);
            socket.close();
        })
    }

    setRoomSocket = (room: string) => {
        const socket = io.connect(`http://localhost:3000/${room}`);
        let totalLag: number[] = [];
    
        socket.on('start', () => {
            sendPostions();
        })
    
        socket.on('opponent-position', (values: { position: number[], player: number, timeStamp: string }) => {
            const now = new Date();
            const sentAt = new Date(values.timeStamp);
            const lag = Math.abs(now.getTime() - sentAt.getTime());
        
            // See what to do with the lag
            totalLag.push(lag);
            if(totalLag.length > 1000){
                console.log(`${this.playerNumber} AVERAGE LAG: ${totalLag.reduce((tot, val) => val+tot, 0) / totalLag.length}`);
                totalLag = [];
            }
        })
    
        const sendPostions = () => {
            const position = [12.34567, 12.34567];
            socket.emit('position', {
                player: this.playerNumber,
                position: position,
                timeStamp: new Date()
            });
        
            setTimeout(sendPostions, 1000/this.requestRate);
        }
    }
}