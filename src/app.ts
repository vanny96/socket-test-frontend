import { SocketClient } from './entities/child';

const maxInstances = 1000;
const requestsPerSecond = 60;

let socketCount = 0;
const createClients = (maxInstances: number, requestsPerSecond: number) => {
    if(socketCount < maxInstances){
        new SocketClient(socketCount, requestsPerSecond);
        socketCount++;
        setTimeout(() => {
            createClients(maxInstances, requestsPerSecond)
        }, 200);
    }
}

createClients(maxInstances, requestsPerSecond);
